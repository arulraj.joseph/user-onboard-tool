import { Component, OnInit } from '@angular/core';
import { LoginService } from 'src/app/common/services/login/login.service';
import { LoginData, Status, Roles } from '../../../common/models';
import { AuthService } from 'src/app/common/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private router: Router, private loginService: LoginService, private auth: AuthService) { }

  ngOnInit() {
    const data: LoginData = {
      password: "mypassword",
      username: "Arulraj"
    };
    this.loginService.login(data).subscribe(resp => {
      if (resp.jwt != null && resp.user != null) {
        this.auth.setAuthInfo(resp);
        
        console.log(resp.user.roles);
        
        if (resp.user.roles.includes(Roles.User)) {
          console.log("regular user");
          this.router.navigate(['/create']);
        }
      }

    });

  }



}
