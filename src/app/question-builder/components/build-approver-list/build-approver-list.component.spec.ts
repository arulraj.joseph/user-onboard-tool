import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuildApproverListComponent } from './build-approver-list.component';

describe('BuildApproverListComponent', () => {
  let component: BuildApproverListComponent;
  let fixture: ComponentFixture<BuildApproverListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuildApproverListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuildApproverListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
