import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { QuestionBuilderComponent } from './pages/question-builder/question-builder.component';
import { ApplicationWorkflowComponent } from './components/application-workflow/application-workflow.component';
import { BuildApproverListComponent } from './components/build-approver-list/build-approver-list.component';
import { QuestionComponent } from './components/question/question.component';



@NgModule({
  declarations: [QuestionBuilderComponent, ApplicationWorkflowComponent, BuildApproverListComponent, QuestionComponent],
  imports: [
    CommonModule
  ]
})
export class QuestionBuilderModule { }
