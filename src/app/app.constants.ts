/**
 * contains app constants
 */
export class AppConstants {

    // the auth token name
    public static AUTH_TOKEN_NAME = 'user_onboard_auth_info';

    // the record status
    public static RECORD_STATUS = {
        active: 'ACTIVE',
        archived: 'ARCHIVED',
        invalid: 'INVALID',
    };

    // the user roles
    public static USER_ROLES = {
        admin: 'admin',
        associate: 'associate',
        approver: 'approver'
    };

    // the record status filter list
    public static RECORD_STATUS_FILTER_LIST = [{ type: 'ACTIVE', checked: false },
    { type: 'ARCHIVED', checked: false },
    { type: 'INVALID', checked: false }];

    // the display type filter list
    public static DISPLAY_TYPE_FILTER_LIST = [{ type: 'Merchandise Bin', checked: false },
    { type: 'Endcap', checked: false }];

    // the pagination options
    public static PAGINATION_OPTIONS = [10, 20, 50, 100];

    // the export table name
    public static EXPORT_TABLE_DATA_FILE_NAME = 'ExportData';

    // the revision count
    public static REVISION_COUNT = 5;
}
