import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestMasterListTableComponent } from './request-master-list-table.component';

describe('RequestMasterListTableComponent', () => {
  let component: RequestMasterListTableComponent;
  let fixture: ComponentFixture<RequestMasterListTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestMasterListTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestMasterListTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
