import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestMasterListComponent } from './request-master-list.component';

describe('RequestMasterListComponent', () => {
  let component: RequestMasterListComponent;
  let fixture: ComponentFixture<RequestMasterListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestMasterListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestMasterListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
