import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RequestMasterListComponent } from './pages/request-master-list/request-master-list.component';
import { RequestMasterListTableComponent } from './components/request-master-list-table/request-master-list-table.component';



@NgModule({
  declarations: [RequestMasterListComponent, RequestMasterListTableComponent],
  imports: [
    CommonModule
  ]
})
export class RequestModule { }
