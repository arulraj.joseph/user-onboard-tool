import { Routes, RouterModule } from '@angular/router';
import { LoginPageComponent } from '../login/pages/login-page/login-page.component';
import { AuthGuard } from '../common/services/auth.guard';
import { CreateApplicationComponent } from '../create-application/pages/create-application/create-application.component';
import { ApplicationReviewComponent } from '../application/components/application-review/application-review.component';
import { QuestionBuilderComponent } from '../question-builder/pages/question-builder/question-builder.component';
import { ApprovalSetupComponent } from '../approval/pages/approval-setup/approval-setup.component';
import { RequestMasterListComponent } from '../request/pages/request-master-list/request-master-list.component';
import { SmtpSettingsComponent } from '../smtp-settings/components/smtp-settings/smtp-settings.component';
import { NgModule } from '@angular/core';

export const routes: Routes = [

  {
    path: 'login',
    component: LoginPageComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'create',
    component: CreateApplicationComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'review',
    component: ApplicationReviewComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'question/builder',
    component: QuestionBuilderComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'approval/setup',
    component: ApprovalSetupComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'request/masterlist',
    component: RequestMasterListComponent,
    canActivate: [AuthGuard]
  },

  {
    path: 'smtp/settings',
    component: SmtpSettingsComponent,
    canActivate: [AuthGuard]
  },

  // otherwise redirect to home
  { path: '**', redirectTo: 'login' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      useHash: false,
      enableTracing: false
    })
  ],
  declarations: [],
  exports: [RouterModule]
})
export class AppRoutingModule { }
