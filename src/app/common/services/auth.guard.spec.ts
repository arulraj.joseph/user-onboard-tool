import { AuthGuard } from './auth.guard';
import { AuthService } from './auth.service';
import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { Router, RouterStateSnapshot, ActivatedRouteSnapshot, Data } from '@angular/router';
import { of } from 'rxjs';

describe('Authuard', () => {

    let authGuard: AuthGuard;
    const authServiceSpy = jasmine.createSpyObj('AuthService', ['isLoggedIn', 'getCurrentUser']);
    const mockRouter = {
        navigate: jasmine.createSpy('navigate')
    };

    beforeEach(() => {


        TestBed.configureTestingModule({
            providers: [
                AuthGuard,
                { provide: AuthService, useValue: authServiceSpy },
                { provide: Router, useValue: mockRouter }
            ],
            imports: [
                HttpClientTestingModule,
                RouterTestingModule
            ]
        });

        authGuard = TestBed.get(AuthGuard);
    });

    it('should create', () => {
        expect(authGuard).toBeDefined();
    });

    it('should return `false` when not authenticated', () => {
        authServiceSpy.isLoggedIn.and.returnValue(false);
        let route;
        route = ActivatedRouteSnapshot;
        let state;
        state = RouterStateSnapshot;
        authGuard.canActivate(route, state).subscribe(canActivate => {
            expect(canActivate).toBe(false);
        });
    });

    it('should return `true` when no role', () => {
        authServiceSpy.isLoggedIn.and.returnValue(true);
        authServiceSpy.getCurrentUser.and.returnValue({ role: 'admin' });
        let route;
        route = ActivatedRouteSnapshot;
        let state;
        state = RouterStateSnapshot;
        authGuard.canActivate(route, state).subscribe(canActivate => {
            expect(canActivate).toBe(true);
        });
    });

    it('should return `true` when no expected role', () => {
        authServiceSpy.isLoggedIn.and.returnValue(true);
        authServiceSpy.getCurrentUser.and.returnValue({ role: 'admin' });
        let state;
        state = RouterStateSnapshot;
        const route: any = {
            data: {
                roles: []
            }
        };
        authGuard.canActivate(route, state).subscribe(canActivate => {
            expect(canActivate).toBe(true);
        });
    });

    it('should return `true` when expected role is there', () => {
        authServiceSpy.isLoggedIn.and.returnValue(true);
        authServiceSpy.getCurrentUser.and.returnValue({ role: 'admin' });
        let state;
        state = RouterStateSnapshot;
        const route: any = {
            data: {
                roles: ['admin']
            }
        };
        authGuard.canActivate(route, state).subscribe(canActivate => {
            expect(canActivate).toBe(true);
        });
    });

    it('should return `false` when expected role is there', () => {
        authServiceSpy.isLoggedIn.and.returnValue(true);
        authServiceSpy.getCurrentUser.and.returnValue({ role: 'admin' });
        let state;
        state = RouterStateSnapshot;
        const route: any = {
            data: {
                roles: ['associate']
            }
        };
        authGuard.canActivate(route, state).subscribe(canActivate => {
            expect(canActivate).toBe(false);
        });
    });
});
