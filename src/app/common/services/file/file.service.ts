import { Injectable } from '@angular/core';
import { File } from '../../models';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class FileService {

  private url = '/files';
  constructor(private httpClient: HttpClient) { }

  /**
   * gets user data
   * @param applicationQuery Application Query Request
   * @returns The observable for the HTTP request.
   */
  public uploadFile(body: File) {
    return this.httpClient.post<File>(
      environment.apiBaseUrl + this.url, body);
  }

  /**
   * gets user data
   * @param applicationQuery Application Query Request
   * @returns The observable for the HTTP request.
   */
  public getFileById(id: string) {
    return this.httpClient.get<File>(
      `${environment.apiBaseUrl + this.url}/${id}`);
  }

  
  /**
   * gets user data
   * @param applicationQuery Application Query Request
   * @returns The observable for the HTTP request.
   */
  public deleteFileById(id: string) {
    return this.httpClient.delete(
      `${environment.apiBaseUrl + this.url}/${id}`);
  }
}
