import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApplicationQueryRequest, ApplicationFormat, ApplicationResponse, CreateApplication, Application, ApplicationApprovalPhases } from '../../../common/models'
import { environment } from 'src/environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class ApplicationService {

  private url = '/applications';
  constructor(private httpClient: HttpClient) { }

  /**
   * gets user data
   * @param applicationQuery Application Query Request
   * @returns The observable for the HTTP request.
   */
  public getApplications(applicationQuery: ApplicationQueryRequest, responseType: string) {
    const httpOptions = {
      params: {
        'responeFormat': applicationQuery.responseFormat.toString(),
        'name': applicationQuery.name,
        'applicationRoleId': applicationQuery.applicationRoleId.toString(),
        'page': applicationQuery.page.toString(),
        'perPage': applicationQuery.perPage.toString(),
        'sortBy': applicationQuery.sortBy,
        'sortOrder': applicationQuery.sortOrder.toString()
      }
    };

    return this.httpClient.get<ApplicationResponse>(
      environment.apiBaseUrl + this.url, httpOptions);
  }

  /**
   * gets user data
   * @param applicationQuery Application Query Request
   * @returns The observable for the HTTP request.
   */
  public createApplication(body: CreateApplication, responseType: string) {
    return this.httpClient.post<ApplicationResponse>(
      environment.apiBaseUrl + this.url, body);
  }

  /**
   * gets user data
   * @param applicationQuery Application Query Request
   * @returns The observable for the HTTP request.
   */
  public getApplicationById(id: string, applicationFormat: ApplicationFormat) {
    const httpOptions = {
      params: {
        'responeFormat': applicationFormat.toString(),
      }
    }

    return this.httpClient.get<ApplicationResponse>(
      `${environment.apiBaseUrl + this.url}/${id}`, httpOptions);
  }

  /**
   * gets user data
   * @param applicationQuery Application Query Request
   * @returns The observable for the HTTP request.
   */
  public updateApplication(id: string, body: Application) {
    return this.httpClient.put<ApplicationResponse>(
      `${environment.apiBaseUrl + this.url}/${id}`, body);
  }

  /**
     * gets user data
     * @param applicationQuery Application Query Request
     * @returns The observable for the HTTP request.
     */
  public getApplicationPhases(id: string) {
    return this.httpClient.get<ApplicationApprovalPhases>(
      `${environment.apiBaseUrl + this.url}/${id}/approvalPhases`);
  }

  /**
   * gets user data
   * @param applicationQuery Application Query Request
   * @returns The observable for the HTTP request.
   */
  public createOrUpdateApplicationPhases(id: string, body: ApplicationApprovalPhases) {
    return this.httpClient.put<ApplicationApprovalPhases>(
      `${environment.apiBaseUrl + this.url}/${id}/approvalPhases`, body);
  }



}

