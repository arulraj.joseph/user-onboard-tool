import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseQueryParam, ApplicationRolesResult } from '../../models';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApplicationRolesLookupService {

  private url = '/applicationRoles';
  constructor(private httpClient: HttpClient) { }

  /**
 * gets user data
 * @param applicationQuery Application Query Request
 * @returns The observable for the HTTP request.
 */
  public getApplicationRoles(name: string, queryParam: BaseQueryParam) {
    const httpOptions = {
      params: {
        'name': name,
        'page': queryParam.page.toString(),
        'perPage': queryParam.perPage.toString(),
        'sortOrder': queryParam.sortOrder.toString()
      }
    }

    return this.httpClient.get<ApplicationRolesResult>(
      environment.apiBaseUrl + this.url, httpOptions);
  }
}
