import { TestBed } from '@angular/core/testing';

import { ApplicationRolesLookupService } from './application-roles-lookup.service';

describe('ApplicationRolesLookupService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ApplicationRolesLookupService = TestBed.get(ApplicationRolesLookupService);
    expect(service).toBeTruthy();
  });
});
