import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { LoginResult, LoginData } from '../../../common/models'

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private url = '/login';
  constructor(private httpClient: HttpClient) { }

  /**
   * Login
   * @param body Login Input Data
   * @returns The observable for the HTTP request.
   */
  public login(body: LoginData) {
    return this.httpClient.post<LoginResult>(environment.apiBaseUrl + this.url, body);
  }

}
