import { Injectable } from '@angular/core';
import { UtilService } from './util.service';
import { AppConstants } from '../../app.constants';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

const storageAuthInfoKeyName = AppConstants.AUTH_TOKEN_NAME;

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private apiUrl = environment.apiBaseUrl;  // URL to web api

  constructor(private util: UtilService,
    private router: Router,
    protected httpClient: HttpClient) { }

  /**
   * handles the logout
   */
  logout() {
    this.router.navigate(['/login']);
    this.util.removeValueFromStorage(storageAuthInfoKeyName);
  }

  /**
   * sets the auth info of user
   * @param authInfo the auth info
   */
  setAuthInfo(authInfo) {
    this.util.storeValueInStorageByKey(storageAuthInfoKeyName, JSON.stringify(authInfo));
  }

  /**
   * gets the current logged in user
   */
  getCurrentUser() {
    return JSON.parse(this.util.getValueFromStorageByKey(storageAuthInfoKeyName));
  }

  /**
   * checks if user is logged in or not
   */
  isLoggedIn() {
    return this.util.getValueFromStorageByKey(storageAuthInfoKeyName) !== null;
  }
}
