import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { UserQuery, UserResult, User } from '../../../common/models';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private url = '/users';
  constructor(private httpClient: HttpClient) { }

  /**
   * gets user data
   * @param userQuery User Query Request
   * @returns The observable for the HTTP request.
   */
  public getUsers(userQuery: UserQuery) {
    const httpOptions = {
      params: {
        'name': userQuery.name,
        'page': userQuery.page.toString(),
        'perPage': userQuery.perPage.toString(),
        'sortBy': userQuery.sortBy,
        'sortOrder': userQuery.sortOrder.toString()
      }
    };

    return this.httpClient.get<UserResult>(
      environment.apiBaseUrl + this.url, httpOptions);

  }

  /**
   * gets user data
   * @param id user entity Id
   * @returns The observable for the HTTP request.
   */
  public getUserById(id: string) {
    return this.httpClient.get<User>(
      `${environment.apiBaseUrl + this.url}/${id}`);
  }
}
