import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SmtpSettings, Email } from '../../models';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SmtpService {



  constructor(private httpClient: HttpClient) { }

  /**
  * gets user data
  * @param applicationQuery Application Query Request
  * @returns The observable for the HTTP request.
  */
  public updateSMTPSettings(smtpSettings: SmtpSettings) {
    return this.httpClient.put<SmtpSettings>(
      `${environment.apiBaseUrl}/smtpSettings`, smtpSettings);
  }

  /**
   * gets user data
   * @param applicationQuery Application Query Request
   * @returns The observable for the HTTP request.
   */
  public getSMTPSettings() {

    return this.httpClient.get<SmtpSettings>(
      `${environment.apiBaseUrl}/smtpSettings`);
  }

  /**
   * gets user data
   * @param applicationQuery Application Query Request
   * @returns The observable for the HTTP request.
   */
  public testEmail(body: Email) {
    return this.httpClient.post(
      `${environment.apiBaseUrl}/testEmail`, body);
  }




}
