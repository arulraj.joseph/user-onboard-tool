import { TestBed } from '@angular/core/testing';
import { UtilService } from './util.service';

describe('Util Service', () => {
  let service: UtilService;
  const storageKeyName = 'dummy';
  const storageValue = 'dummy';

  /**
   * handler for each test start. initializes the environment
   */
  beforeEach(() => {
    let store = {};
    const mockLocalStorage = {
      getItem: (key: string): string => {
        return key in store ? store[key] : null;
      },
      setItem: (key: string, value: string) => {
        store[key] = `${value}`;
      },
      removeItem: (key: string) => {
        delete store[key];
      },
      clear: () => {
        store = {};
      }
    };
    spyOn(localStorage, 'getItem')
      .and.callFake(mockLocalStorage.getItem);
    spyOn(localStorage, 'setItem')
      .and.callFake(mockLocalStorage.setItem);
    spyOn(localStorage, 'removeItem')
      .and.callFake(mockLocalStorage.removeItem);
    spyOn(localStorage, 'clear')
      .and.callFake(mockLocalStorage.clear);
  });

  describe('should be created', () => {
    it('should be created', () => {
      service = TestBed.get(UtilService);
      expect(service).toBeTruthy();
    });
  });

  describe('verify `removeValueFromStorage` method', () => {
    it('should remove item from localstorage', () => {
      service = TestBed.get(UtilService);
      localStorage.setItem(storageKeyName, storageValue);
      service.removeValueFromStorage(storageKeyName);
      expect(localStorage.getItem(storageKeyName)).toEqual(null);
    });
  });

  describe('verify `storeValueInStorageByKey` method', () => {
    it('should store value in localstorage', () => {
      service = TestBed.get(UtilService);
      service.storeValueInStorageByKey(storageKeyName, storageValue);
      expect(localStorage.getItem(storageKeyName)).toEqual(storageValue);
    });
  });

  describe('verify `getValueFromStorageByKey` method', () => {
    it('should get value in localstorage', () => {
      service = TestBed.get(UtilService);
      localStorage.setItem(storageKeyName, storageValue);
      expect(localStorage.getItem(storageKeyName)).toEqual(service.getValueFromStorageByKey(storageKeyName));
    });
  });

  describe('verify `convertCamelCaseToUpperCase` method', () => {
    it('should convert camel case to upper case successfully', () => {
      service = TestBed.get(UtilService);
      expect(service.convertCamelCaseToUpperCase('recordKey')).toEqual('Record Key');
    });
  });
});
