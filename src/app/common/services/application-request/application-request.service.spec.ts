import { TestBed } from '@angular/core/testing';

import { ApplicationRequestService } from './application-request.service';

describe('ApplicationRequestService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ApplicationRequestService = TestBed.get(ApplicationRequestService);
    expect(service).toBeTruthy();
  });
});
