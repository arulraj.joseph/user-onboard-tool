import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApplicationQueryRequests, ApplicationRequestsResponse, CreateApplicationRequests, ApplicationRequest, ApplicationRequestMetaData, ApplicationRequestReview, Status, BaseQueryParam } from '../../models';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApplicationRequestService {

  private url = '/applicationRequests';
  constructor(private httpClient: HttpClient) { }

  /**
   * gets user data
   * @param applicationQuery Application Query Request
   * @returns The observable for the HTTP request.
   */
  public getApplications(applicationQueryRequests: ApplicationQueryRequests) {
    const httpOptions = {
      params: {
        'applicationId': applicationQueryRequests.applicationId.toString(),
        'userId': applicationQueryRequests.userId.toString(),
        'status': applicationQueryRequests.status.toString(),

        'createdOnStartDate': applicationQueryRequests.createdOnStartDate,
        'createdOnEndDate': applicationQueryRequests.createdOnEndDate,
        'approvalStartDate': applicationQueryRequests.approvalStartDate,
        'approvalEndDate': applicationQueryRequests.approvalEndDate,

        'page': applicationQueryRequests.page.toString(),
        'perPage': applicationQueryRequests.perPage.toString(),
        'sortBy': applicationQueryRequests.sortBy.toString(),
        'sortOrder': applicationQueryRequests.sortOrder.toString()
      }
    };

    return this.httpClient.get<ApplicationRequestsResponse>(
      environment.apiBaseUrl + this.url, httpOptions);
  }


  /**
   * gets user data
   * @param applicationQuery Application Query Request
   * @returns The observable for the HTTP request.
   */
  public createApplicationRequests(body: CreateApplicationRequests) {
    return this.httpClient.post<ApplicationRequest>(
      environment.apiBaseUrl + this.url, body);
  }

  /**
   * gets user data
   * @param applicationQuery Application Query Request
   * @returns The observable for the HTTP request.
   */
  public getApplicationRequestsById(id: string, responseFormat: string) {
    return this.httpClient.get<ApplicationRequest | ApplicationRequestMetaData>(
      `${environment.apiBaseUrl + this.url}/${id}?responseFormat=${responseFormat}`);
  }

  /**
   * gets user data
   * @param applicationQuery Application Query Request
   * @returns The observable for the HTTP request.
   */
  public updateApplicationRequests(body: ApplicationRequest) {
    return this.httpClient.put<ApplicationRequest>(
      environment.apiBaseUrl + this.url, body);
  }

  /**
   * gets user data
   * @param applicationQuery Application Query Request
   * @returns The observable for the HTTP request.
   */
  public createReview(id: string, body: ApplicationRequestReview) {
    return this.httpClient.post<ApplicationRequest>(
      `${environment.apiBaseUrl + this.url}/${id}/reviews`, body);
  }

  /**
     * gets user data
     * @param applicationQuery Application Query Request
     * @returns The observable for the HTTP request.
     */
  public getReview(id: string, status: Status, queryParams: BaseQueryParam) {
    const httpOptions = {
      params: {
        'status':Status.Approved.toString(),
        'page' : queryParams.page.toString(),
        'perPage' : queryParams.perPage.toString(),
        'sortBy' : queryParams.sortBy,
        'sortOrder' : queryParams.sortOrder.toString()
      }
    }
    return this.httpClient.get<ApplicationRequestsResponse>(
      `${environment.apiBaseUrl + this.url}/${id}/reviews`, httpOptions);
  }
}
