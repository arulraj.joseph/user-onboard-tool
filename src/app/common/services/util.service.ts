import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UtilService {

  constructor() { }

  /**
   * removes the value from local storage
   * @param key the key
   */
  removeValueFromStorage(key) {
    localStorage.removeItem(key);
  }

  /**
   * stores the value in local storage
   * @param key the key
   * @param value the value
   */
  storeValueInStorageByKey(key, value) {
    localStorage.setItem(key, value);
  }

  /**
   * gets the value by key from local storage
   * @param key the key
   */
  getValueFromStorageByKey(key) {
    return localStorage.getItem(key);
  }

  /**
   * convert the text to upper case from camel case
   * @param text the text
   */
  convertCamelCaseToUpperCase(text) {
    return text.replace(/([A-Z])/g, ' $1')
      .replace(/^./, function (str) { return str.toUpperCase(); });
  }
}
