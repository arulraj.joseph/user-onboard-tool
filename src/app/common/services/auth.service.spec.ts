import { TestBed } from '@angular/core/testing';
import { AuthService } from './auth.service';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { AppConstants } from '../../app.constants';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';
import { HttpClient } from '@angular/common/http';

const storageAuthInfoKeyName = AppConstants.AUTH_TOKEN_NAME;

describe('Auth Service', () => {
    let service: AuthService;

    const mockRouter = {
        navigate: jasmine.createSpy('navigate')
    };
    let httpClientMock: any;
    httpClientMock = jasmine.createSpyObj('httpClientMock', ['get']);
    const mockLoginData: any = [
        {
            userName: 'associate@abc.com',
            password: '123',
            userRole: 'Associate',
            token: 'token'
        },
        {
            userName: 'approver@abc.com',
            password: '123',
            userRole: 'Approver',
            token: 'token'
        },
        {
            userName: 'admin@abc.com',
            password: '123',
            userRole: 'Admin',
            token: 'token'
        }
    ];
    httpClientMock.get.and.returnValue(of(mockLoginData));

    /**
     * handler for each test start. initializes the environment
     */
    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [ToastrModule.forRoot({
                timeOut: 3000,
                positionClass: 'toast-bottom-right',
                closeButton: true,
                autoDismiss: false
            }),
                HttpClientTestingModule],
            providers: [ToastrService,
                { provide: Router, useValue: mockRouter },
                { provide: HttpClient, useValue: httpClientMock }
            ]
        }).compileComponents();
    });

    describe('should be created', () => {
        it('should be created', () => {
            service = TestBed.get(AuthService);
            expect(service).toBeTruthy();
        });
    });

    describe('verify `logout` method', () => {
        it('should logout and go to login page', () => {
            service = TestBed.get(AuthService);
            service.logout();
            expect(mockRouter.navigate).toHaveBeenCalledWith(['/login']);
        });
    });

    describe('verify `setAuthInfo` method', () => {
        it('should set the auth info', () => {
            service = TestBed.get(AuthService);
            service.setAuthInfo('test');
            expect(localStorage.getItem(storageAuthInfoKeyName)).toBe('"test"');
        });
    });

    describe('verify `setAuthInfo` method', () => {
        it('should set the auth info', () => {
            service = TestBed.get(AuthService);
            service.setAuthInfo('test');
            expect(localStorage.getItem(storageAuthInfoKeyName)).toBe('"test"');
            service.logout();
        });
    });

    describe('verify `getCurrentUser` method', () => {
        it('should get the current user successfully', () => {
            service = TestBed.get(AuthService);
            service.getCurrentUser();
            service.setAuthInfo('test');
            expect(JSON.parse(localStorage.getItem(storageAuthInfoKeyName))).toBe(service.getCurrentUser());
            service.logout();
        });
    });

    describe('verify `isLoggedIn` method', () => {
        it('should get the info if user is logged in or not', () => {
            service = TestBed.get(AuthService);
            expect(service.isLoggedIn()).toBe(false);
        });
    });

    describe('verify `getLoginInfo` method', () => {
        it('should get the data', () => {
            service = TestBed.get(AuthService);
            service.getLoginInfo().subscribe(res => {
                expect(res).toBe(mockLoginData);
            });
        });
    });
});
