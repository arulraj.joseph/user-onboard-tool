import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SideNavComponent } from './side-nav/side-nav.component';
import { AuthGuard } from './services/auth.guard';



@NgModule({
  declarations: [SideNavComponent],
  imports: [
    CommonModule
  ]
})
export class SharedModule { }
