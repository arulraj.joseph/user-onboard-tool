export interface ApplicationRole {
    id: number;
    name: string;
}

export interface ApplicationRolesResult {
    results: ApplicationRole[],
    total: number,
    page: number,
    perPage: number
}

export interface LoginData {
    username: string;
    password: string;
}

export interface LoginResult {
    jwt: string;
    user: User;
}

export interface UserQuery extends BaseQueryParam {
    name: string;
}

export interface UserResult {
    results: User[];
    total: number;
    page: number;
    perPage: number;
}

export interface User {
    id: number;
    username: string;
    roles: Roles;
    fullName?: string;
    jobTitle?: string;
    phoneNumberCountryCode?: string;
    phoneNumber?: string;
    email?: string;
    salaryNumber?: number;
    workstationId?: string;
    managersFullName?: string;
    profilePictureUrl?: string;
    createdOn?: string;
    updatedOn?: string
}

export interface BaseQueryParam {
    page: number;
    perPage: number;
    sortBy: any;
    sortOrder: sortOrder;
}

export interface ApplicationQueryRequest extends BaseQueryParam {
    responseFormat: ApplicationFormat;
    name: string;
    applicationRoleId: number;
}

export interface ApplicationQueryRequests extends BaseQueryParam {
    applicationId?: number;
    userId?: number;
    status?: Status;
    createdOnStartDate?: string;
    createdOnEndDate?: string;
    approvalStartDate?: string;
    approvalEndDate?: string;
    sortBy: ApplicationRequestsSortBy.applicationName
}

export interface ApplicationRequestsResponse {
    results: ApplicationRequestMetaData[] | ApplicationRequestReview[];
    total: number;
    page: number;
    perPage: number;
}

export interface UserMetaData {
    id: number;
    fullName: string;
    profilePictureUrl?: string;
}

export interface ApplicationRequest {
    id?: number;
    fromUser?: User;
    application?: Application;
    answers: Answer[];
    status?: Status;
    customerProfiles?: string;
    roleJustification?: string;
    updatedBy?: number;
    createdOn?: string;
    updatedOn?: string;
}

export interface CreateApplicationRequests {
    user: object;
    applicationRequests: ApplicationRequest[]
}

export interface ApplicationResponse {
    results: Application | ApplicationMetaData | ApplicationMetadataWithApprovers
}

export interface Answer {
    description?: string;
    question?: Question;
    selectedOptionValues?: string[];
    text?: string;
    selectedDate?: string;
    file?: File;
}

export interface Question {
    id?: number;
    questionPublicId: string;
    title: string;
    type: string;
    description?: string;
    required: Boolean;
    options?: Option[];
    conditionalQuestions?: ConditionalQuestion[];
    conditionalWarnings?: ConditionalWarning[];
    section?: string;
}

export interface ConditionalQuestion {
    selectedOptionValue?: string;
    enteredText?: string;
    selectedDate?: string;
    dependentQuestionPublicIds?: number[];
}

export interface ConditionalWarning {
    selectedOptionValue?: string;
    enteredText?: string;
    selectedDate?: string;
    warningMessages?: string[];
}

export interface Application {
    id?: number;
    name?: string;
    applicationRoles?: ApplicationRole[];
    questions?: Question[];
    totalNumberOfQuestions?: number;
    createdBy?: number;
    updatedBy?: number;
    createdOn?: string;
    updatedOn?: string;
}

export interface CreateApplication {
    name?: string;
    applicationRoles?: ApplicationRole[];
    questions?: Question[];
}


export interface Option {
    text?: string;
    value?: string;
}

export interface ApplicationRequestMetaData {
    id?: number;
    fromUser: UserMetaData;
    application: Application;
    approvers: UserMetaData[];
    status: Status;
    createdOn: string;
    approvalDate?: string;
}

export interface ApplicationRequestReview {
    id?: number;
    approverId: number;
    applicationRequestId: number;
    status: Status;
    rejectionReason?: string;
    createdOn?: string;
    updatedOn?: string;
}

export interface ApplicationMetaData {
    id: number;
    name: string;
    totalNumberOfQuestions: number;
    createdBy: UserMetaData;
    UpdatedBy?: UserMetaData;
    createdOn: string;
    UpdatedOn?: string;
}


export interface ApplicationMetadataWithApprovers extends ApplicationMetaData {
    approvers: UserMetaData;
}

export interface ApplicationApprovalPhases {
    approvalPhases: ApprovalPhase[];
}

export interface ApprovalPhase {
    id: number;
    phaseNumber?: number;
    approvalCondition: ApprovalCondition;
    approvers: UserMetaData;
}

export interface SmtpSettings {
    serverAddress: string;
    serverPort: number;
    username: string;
    password: string;
    advancedEmail?: string;
    useTls: boolean;
    useExtendedGreeting: boolean;
}

export interface Email {
    to: string[],
    subject: string,
    body?: string
}

export interface File {
    id?: number;
    name: string;
    mimeType: string;
    content: string;
    createdOn?: string;
    createdBy?: number;
}

export enum ApplicationFormat {
    Application,
    ApplicationMetaData,
    ApplicationMetadataWithApprovers
}

export enum Roles {
    User = "User",
    Approver = "Approver",
    Admin = "Admin"
}

export enum Status {
    Pending = 0,
    Approved = 1,
    Rejected = -1
}


enum UserSortBy {
    name,
    status,
    autoPublishOn
}

enum ApplicationRequestsSortBy {
    applicationName,
    createdOn,
    totalQuestions,
    userFullName,
    approvalDate,
    status
}

enum sortOrder {
    asc,
    desc
}

enum ApprovalCondition {
    All,
    Any
}
