import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreateApplicationComponent } from './pages/create-application/create-application.component';
import { UserInformationComponent } from './components/user-information/user-information.component';
import { AnswerQuestionsComponent } from './components/answer-questions/answer-questions.component';



@NgModule({
  declarations: [CreateApplicationComponent, UserInformationComponent, AnswerQuestionsComponent],
  imports: [
    CommonModule
  ]
})
export class CreateApplicationModule { }
