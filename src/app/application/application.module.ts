import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ApplicationReviewComponent } from './components/application-review/application-review.component';
import { ApplicationComponent } from './pages/application/application.component';
import { CustomerInfoComponent } from './components/customer-info/customer-info.component';



@NgModule({
  declarations: [ApplicationReviewComponent, ApplicationComponent, CustomerInfoComponent],
  imports: [
    CommonModule
  ]
})
export class ApplicationModule { }
