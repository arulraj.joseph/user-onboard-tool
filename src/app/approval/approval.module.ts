import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ApprovalSetupComponent } from './pages/approval-setup/approval-setup.component';



@NgModule({
  declarations: [ApprovalSetupComponent],
  imports: [
    CommonModule
  ]
})
export class ApprovalModule { }
