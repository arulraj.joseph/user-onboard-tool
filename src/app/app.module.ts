import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './routing/routing.module';
import { AppComponent } from './app.component';
import { LoginModule } from './login/login.module';
import { ApplicationModule } from './application/application.module';
import { SmtpSettingsModule } from './smtp-settings/smtp-settings.module';
import { ApprovalModule } from './approval/approval.module';
import { CreateApplicationModule } from './create-application/apply-application.module';
import { QuestionBuilderModule } from './question-builder/question-builder.module';
import { RequestModule } from './request/request.module';
import { SharedModule } from './common/shared.module';
import { AuthGuard } from './common/services/auth.guard';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    SharedModule,
    LoginModule,
    ApplicationModule,
    ApprovalModule,
    CreateApplicationModule,
    QuestionBuilderModule,
    RequestModule,
    AppRoutingModule,
    SmtpSettingsModule,
  ],
  providers: [AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
