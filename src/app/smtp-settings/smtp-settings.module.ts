import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SettingsComponent } from './pages/settings/settings.component';
import { ApprovalWorkflowComponent } from './components/approval-workflow/approval-workflow.component';
import { SmtpSettingsComponent } from './components/smtp-settings/smtp-settings.component';



@NgModule({
  declarations: [SettingsComponent, ApprovalWorkflowComponent, SmtpSettingsComponent],
  imports: [
    CommonModule
  ]
})
export class SmtpSettingsModule { }
